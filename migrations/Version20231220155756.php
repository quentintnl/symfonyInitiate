<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231220155756 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE recette (id INT AUTO_INCREMENT NOT NULL, id_categorie_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, temps_preparation VARCHAR(255) NOT NULL, temps_cuisson VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_49BB63909F34925F (id_categorie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE recette_recette_sous_categorie (recette_id INT NOT NULL, recette_sous_categorie_id INT NOT NULL, INDEX IDX_3D0D4AD089312FE9 (recette_id), INDEX IDX_3D0D4AD024C05003 (recette_sous_categorie_id), PRIMARY KEY(recette_id, recette_sous_categorie_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rel_recette_ingredients (id INT AUTO_INCREMENT NOT NULL, id_recette_id INT NOT NULL, id_ingredient_id INT NOT NULL, id_unite_id INT NOT NULL, quantite VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_3ABE1122CBBAF3E (id_recette_id), UNIQUE INDEX UNIQ_3ABE1122D1731E9 (id_ingredient_id), UNIQUE INDEX UNIQ_3ABE112BC91983E (id_unite_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rel_recette_instructions (id INT AUTO_INCREMENT NOT NULL, id_recette_id INT NOT NULL, id_instructions_id INT NOT NULL, ordre VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8CEF67922CBBAF3E (id_recette_id), UNIQUE INDEX UNIQ_8CEF6792B72B0BAB (id_instructions_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rel_recette_ustensiles (id INT AUTO_INCREMENT NOT NULL, id_recette_id INT NOT NULL, id_ustensiles_id INT NOT NULL, quantite VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_E87FF2A92CBBAF3E (id_recette_id), UNIQUE INDEX UNIQ_E87FF2A95A58040E (id_ustensiles_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE unite (id INT AUTO_INCREMENT NOT NULL, id_categorie_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_1D64C1189F34925F (id_categorie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE recette ADD CONSTRAINT FK_49BB63909F34925F FOREIGN KEY (id_categorie_id) REFERENCES recette_categorie (id)');
        $this->addSql('ALTER TABLE recette_recette_sous_categorie ADD CONSTRAINT FK_3D0D4AD089312FE9 FOREIGN KEY (recette_id) REFERENCES recette (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE recette_recette_sous_categorie ADD CONSTRAINT FK_3D0D4AD024C05003 FOREIGN KEY (recette_sous_categorie_id) REFERENCES recette_sous_categorie (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE rel_recette_ingredients ADD CONSTRAINT FK_3ABE1122CBBAF3E FOREIGN KEY (id_recette_id) REFERENCES recette (id)');
        $this->addSql('ALTER TABLE rel_recette_ingredients ADD CONSTRAINT FK_3ABE1122D1731E9 FOREIGN KEY (id_ingredient_id) REFERENCES ingredients (id)');
        $this->addSql('ALTER TABLE rel_recette_ingredients ADD CONSTRAINT FK_3ABE112BC91983E FOREIGN KEY (id_unite_id) REFERENCES unite (id)');
        $this->addSql('ALTER TABLE rel_recette_instructions ADD CONSTRAINT FK_8CEF67922CBBAF3E FOREIGN KEY (id_recette_id) REFERENCES recette (id)');
        $this->addSql('ALTER TABLE rel_recette_instructions ADD CONSTRAINT FK_8CEF6792B72B0BAB FOREIGN KEY (id_instructions_id) REFERENCES instructions (id)');
        $this->addSql('ALTER TABLE rel_recette_ustensiles ADD CONSTRAINT FK_E87FF2A92CBBAF3E FOREIGN KEY (id_recette_id) REFERENCES recette (id)');
        $this->addSql('ALTER TABLE rel_recette_ustensiles ADD CONSTRAINT FK_E87FF2A95A58040E FOREIGN KEY (id_ustensiles_id) REFERENCES utensils (id)');
        $this->addSql('ALTER TABLE unite ADD CONSTRAINT FK_1D64C1189F34925F FOREIGN KEY (id_categorie_id) REFERENCES unite_categorie (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE recette DROP FOREIGN KEY FK_49BB63909F34925F');
        $this->addSql('ALTER TABLE recette_recette_sous_categorie DROP FOREIGN KEY FK_3D0D4AD089312FE9');
        $this->addSql('ALTER TABLE recette_recette_sous_categorie DROP FOREIGN KEY FK_3D0D4AD024C05003');
        $this->addSql('ALTER TABLE rel_recette_ingredients DROP FOREIGN KEY FK_3ABE1122CBBAF3E');
        $this->addSql('ALTER TABLE rel_recette_ingredients DROP FOREIGN KEY FK_3ABE1122D1731E9');
        $this->addSql('ALTER TABLE rel_recette_ingredients DROP FOREIGN KEY FK_3ABE112BC91983E');
        $this->addSql('ALTER TABLE rel_recette_instructions DROP FOREIGN KEY FK_8CEF67922CBBAF3E');
        $this->addSql('ALTER TABLE rel_recette_instructions DROP FOREIGN KEY FK_8CEF6792B72B0BAB');
        $this->addSql('ALTER TABLE rel_recette_ustensiles DROP FOREIGN KEY FK_E87FF2A92CBBAF3E');
        $this->addSql('ALTER TABLE rel_recette_ustensiles DROP FOREIGN KEY FK_E87FF2A95A58040E');
        $this->addSql('ALTER TABLE unite DROP FOREIGN KEY FK_1D64C1189F34925F');
        $this->addSql('DROP TABLE recette');
        $this->addSql('DROP TABLE recette_recette_sous_categorie');
        $this->addSql('DROP TABLE rel_recette_ingredients');
        $this->addSql('DROP TABLE rel_recette_instructions');
        $this->addSql('DROP TABLE rel_recette_ustensiles');
        $this->addSql('DROP TABLE unite');
    }
}
