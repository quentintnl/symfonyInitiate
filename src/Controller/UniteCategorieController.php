<?php

namespace App\Controller;

use App\Entity\UniteCategorie;
use App\Form\UniteCategorieType;
use App\Repository\UniteCategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/unite/categorie')]
class UniteCategorieController extends AbstractController
{
    #[Route('/', name: 'app_unite_categorie_index', methods: ['GET'])]
    public function index(UniteCategorieRepository $uniteCategorieRepository): Response
    {
        return $this->render('unite_categorie/index.html.twig', [
            'unite_categories' => $uniteCategorieRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_unite_categorie_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $uniteCategorie = new UniteCategorie();
        $form = $this->createForm(UniteCategorieType::class, $uniteCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($uniteCategorie);
            $entityManager->flush();

            return $this->redirectToRoute('app_unite_categorie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('unite_categorie/new.html.twig', [
            'unite_categorie' => $uniteCategorie,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_unite_categorie_show', methods: ['GET'])]
    public function show(UniteCategorie $uniteCategorie): Response
    {
        return $this->render('unite_categorie/show.html.twig', [
            'unite_categorie' => $uniteCategorie,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_unite_categorie_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, UniteCategorie $uniteCategorie, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(UniteCategorieType::class, $uniteCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_unite_categorie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('unite_categorie/edit.html.twig', [
            'unite_categorie' => $uniteCategorie,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_unite_categorie_delete', methods: ['POST'])]
    public function delete(Request $request, UniteCategorie $uniteCategorie, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$uniteCategorie->getId(), $request->request->get('_token'))) {
            $entityManager->remove($uniteCategorie);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_unite_categorie_index', [], Response::HTTP_SEE_OTHER);
    }
}
