<?php

namespace App\Controller;

use App\Entity\RecetteCategorie;
use App\Form\RecetteCategorieType;
use App\Repository\RecetteCategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/recette/categorie')]
class RecetteCategorieController extends AbstractController
{
    #[Route('/', name: 'app_recette_categorie_index', methods: ['GET'])]
    public function index(RecetteCategorieRepository $recetteCategorieRepository): Response
    {
        return $this->render('recette_categorie/index.html.twig', [
            'recette_categories' => $recetteCategorieRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_recette_categorie_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $recetteCategorie = new RecetteCategorie();
        $form = $this->createForm(RecetteCategorieType::class, $recetteCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($recetteCategorie);
            $entityManager->flush();

            return $this->redirectToRoute('app_recette_categorie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('recette_categorie/new.html.twig', [
            'recette_categorie' => $recetteCategorie,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_recette_categorie_show', methods: ['GET'])]
    public function show(RecetteCategorie $recetteCategorie): Response
    {
        return $this->render('recette_categorie/show.html.twig', [
            'recette_categorie' => $recetteCategorie,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_recette_categorie_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RecetteCategorie $recetteCategorie, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RecetteCategorieType::class, $recetteCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_recette_categorie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('recette_categorie/edit.html.twig', [
            'recette_categorie' => $recetteCategorie,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_recette_categorie_delete', methods: ['POST'])]
    public function delete(Request $request, RecetteCategorie $recetteCategorie, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$recetteCategorie->getId(), $request->request->get('_token'))) {
            $entityManager->remove($recetteCategorie);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_recette_categorie_index', [], Response::HTTP_SEE_OTHER);
    }
}
