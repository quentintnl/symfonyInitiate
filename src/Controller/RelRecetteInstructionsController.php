<?php

namespace App\Controller;

use App\Entity\RelRecetteInstructions;
use App\Form\RelRecetteInstructionsType;
use App\Repository\RelRecetteInstructionsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/rel/recette/instructions')]
class RelRecetteInstructionsController extends AbstractController
{
    #[Route('/', name: 'app_rel_recette_instructions_index', methods: ['GET'])]
    public function index(RelRecetteInstructionsRepository $relRecetteInstructionsRepository): Response
    {
        return $this->render('rel_recette_instructions/index.html.twig', [
            'rel_recette_instructions' => $relRecetteInstructionsRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_rel_recette_instructions_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $relRecetteInstruction = new RelRecetteInstructions();
        $form = $this->createForm(RelRecetteInstructionsType::class, $relRecetteInstruction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($relRecetteInstruction);
            $entityManager->flush();

            return $this->redirectToRoute('app_rel_recette_instructions_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('rel_recette_instructions/new.html.twig', [
            'rel_recette_instruction' => $relRecetteInstruction,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_rel_recette_instructions_show', methods: ['GET'])]
    public function show(RelRecetteInstructions $relRecetteInstruction): Response
    {
        return $this->render('rel_recette_instructions/show.html.twig', [
            'rel_recette_instruction' => $relRecetteInstruction,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_rel_recette_instructions_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RelRecetteInstructions $relRecetteInstruction, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RelRecetteInstructionsType::class, $relRecetteInstruction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_rel_recette_instructions_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('rel_recette_instructions/edit.html.twig', [
            'rel_recette_instruction' => $relRecetteInstruction,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_rel_recette_instructions_delete', methods: ['POST'])]
    public function delete(Request $request, RelRecetteInstructions $relRecetteInstruction, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$relRecetteInstruction->getId(), $request->request->get('_token'))) {
            $entityManager->remove($relRecetteInstruction);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_rel_recette_instructions_index', [], Response::HTTP_SEE_OTHER);
    }
}
