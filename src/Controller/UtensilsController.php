<?php

namespace App\Controller;

use App\Entity\Utensils;
use App\Form\UtensilsType;
use App\Repository\UtensilsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/utensils')]
class UtensilsController extends AbstractController
{
    #[Route('/', name: 'app_utensils_index', methods: ['GET'])]
    public function index(UtensilsRepository $utensilsRepository): Response
    {
        return $this->render('utensils/index.html.twig', [
            'utensils' => $utensilsRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_utensils_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $utensil = new Utensils();
        $form = $this->createForm(UtensilsType::class, $utensil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($utensil);
            $entityManager->flush();

            return $this->redirectToRoute('app_utensils_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('utensils/new.html.twig', [
            'utensil' => $utensil,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_utensils_show', methods: ['GET'])]
    public function show(Utensils $utensil): Response
    {
        return $this->render('utensils/show.html.twig', [
            'utensil' => $utensil,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_utensils_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Utensils $utensil, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(UtensilsType::class, $utensil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_utensils_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('utensils/edit.html.twig', [
            'utensil' => $utensil,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_utensils_delete', methods: ['POST'])]
    public function delete(Request $request, Utensils $utensil, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$utensil->getId(), $request->request->get('_token'))) {
            $entityManager->remove($utensil);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_utensils_index', [], Response::HTTP_SEE_OTHER);
    }
}
