<?php

namespace App\Controller;

use App\Entity\RelRecetteUstensiles;
use App\Form\RelRecetteUstensilesType;
use App\Repository\RelRecetteUstensilesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/rel/recette/ustensiles')]
class RelRecetteUstensilesController extends AbstractController
{
    #[Route('/', name: 'app_rel_recette_ustensiles_index', methods: ['GET'])]
    public function index(RelRecetteUstensilesRepository $relRecetteUstensilesRepository): Response
    {
        return $this->render('rel_recette_ustensiles/index.html.twig', [
            'rel_recette_ustensiles' => $relRecetteUstensilesRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_rel_recette_ustensiles_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $relRecetteUstensile = new RelRecetteUstensiles();
        $form = $this->createForm(RelRecetteUstensilesType::class, $relRecetteUstensile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($relRecetteUstensile);
            $entityManager->flush();

            return $this->redirectToRoute('app_rel_recette_ustensiles_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('rel_recette_ustensiles/new.html.twig', [
            'rel_recette_ustensile' => $relRecetteUstensile,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_rel_recette_ustensiles_show', methods: ['GET'])]
    public function show(RelRecetteUstensiles $relRecetteUstensile): Response
    {
        return $this->render('rel_recette_ustensiles/show.html.twig', [
            'rel_recette_ustensile' => $relRecetteUstensile,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_rel_recette_ustensiles_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RelRecetteUstensiles $relRecetteUstensile, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RelRecetteUstensilesType::class, $relRecetteUstensile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_rel_recette_ustensiles_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('rel_recette_ustensiles/edit.html.twig', [
            'rel_recette_ustensile' => $relRecetteUstensile,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_rel_recette_ustensiles_delete', methods: ['POST'])]
    public function delete(Request $request, RelRecetteUstensiles $relRecetteUstensile, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$relRecetteUstensile->getId(), $request->request->get('_token'))) {
            $entityManager->remove($relRecetteUstensile);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_rel_recette_ustensiles_index', [], Response::HTTP_SEE_OTHER);
    }
}
