<?php

namespace App\Controller;

use App\Entity\RecetteSousCategorie;
use App\Form\RecetteSousCategorieType;
use App\Repository\RecetteSousCategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/recette/sous/categorie')]
class RecetteSousCategorieController extends AbstractController
{
    #[Route('/', name: 'app_recette_sous_categorie_index', methods: ['GET'])]
    public function index(RecetteSousCategorieRepository $recetteSousCategorieRepository): Response
    {
        return $this->render('recette_sous_categorie/index.html.twig', [
            'recette_sous_categories' => $recetteSousCategorieRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_recette_sous_categorie_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $recetteSousCategorie = new RecetteSousCategorie();
        $form = $this->createForm(RecetteSousCategorieType::class, $recetteSousCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($recetteSousCategorie);
            $entityManager->flush();

            return $this->redirectToRoute('app_recette_sous_categorie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('recette_sous_categorie/new.html.twig', [
            'recette_sous_categorie' => $recetteSousCategorie,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_recette_sous_categorie_show', methods: ['GET'])]
    public function show(RecetteSousCategorie $recetteSousCategorie): Response
    {
        return $this->render('recette_sous_categorie/show.html.twig', [
            'recette_sous_categorie' => $recetteSousCategorie,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_recette_sous_categorie_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RecetteSousCategorie $recetteSousCategorie, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RecetteSousCategorieType::class, $recetteSousCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_recette_sous_categorie_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('recette_sous_categorie/edit.html.twig', [
            'recette_sous_categorie' => $recetteSousCategorie,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_recette_sous_categorie_delete', methods: ['POST'])]
    public function delete(Request $request, RecetteSousCategorie $recetteSousCategorie, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$recetteSousCategorie->getId(), $request->request->get('_token'))) {
            $entityManager->remove($recetteSousCategorie);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_recette_sous_categorie_index', [], Response::HTTP_SEE_OTHER);
    }
}
