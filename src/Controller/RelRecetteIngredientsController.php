<?php

namespace App\Controller;

use App\Entity\RelRecetteIngredients;
use App\Form\RelRecetteIngredientsType;
use App\Repository\RelRecetteIngredientsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/rel/recette/ingredients')]
class RelRecetteIngredientsController extends AbstractController
{
    #[Route('/', name: 'app_rel_recette_ingredients_index', methods: ['GET'])]
    public function index(RelRecetteIngredientsRepository $relRecetteIngredientsRepository): Response
    {
        return $this->render('rel_recette_ingredients/index.html.twig', [
            'rel_recette_ingredients' => $relRecetteIngredientsRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_rel_recette_ingredients_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $relRecetteIngredient = new RelRecetteIngredients();
        $form = $this->createForm(RelRecetteIngredientsType::class, $relRecetteIngredient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($relRecetteIngredient);
            $entityManager->flush();

            return $this->redirectToRoute('app_rel_recette_ingredients_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('rel_recette_ingredients/new.html.twig', [
            'rel_recette_ingredient' => $relRecetteIngredient,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_rel_recette_ingredients_show', methods: ['GET'])]
    public function show(RelRecetteIngredients $relRecetteIngredient): Response
    {
        return $this->render('rel_recette_ingredients/show.html.twig', [
            'rel_recette_ingredient' => $relRecetteIngredient,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_rel_recette_ingredients_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, RelRecetteIngredients $relRecetteIngredient, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RelRecetteIngredientsType::class, $relRecetteIngredient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_rel_recette_ingredients_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('rel_recette_ingredients/edit.html.twig', [
            'rel_recette_ingredient' => $relRecetteIngredient,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_rel_recette_ingredients_delete', methods: ['POST'])]
    public function delete(Request $request, RelRecetteIngredients $relRecetteIngredient, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$relRecetteIngredient->getId(), $request->request->get('_token'))) {
            $entityManager->remove($relRecetteIngredient);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_rel_recette_ingredients_index', [], Response::HTTP_SEE_OTHER);
    }
}
