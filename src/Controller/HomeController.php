<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    #[Route('/recette{recipe}', name: 'app_home')]
    public function index(Request $request): Response
    {
        $parametre = $request->get('recipe');

        return $this->render('home/' . $parametre . '.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
}
