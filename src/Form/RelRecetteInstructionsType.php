<?php

namespace App\Form;

use App\Entity\Instructions;
use App\Entity\Recette;
use App\Entity\RelRecetteInstructions;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RelRecetteInstructionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ordre')
            ->add('id_recette', EntityType::class, [
                'class' => Recette::class,
'choice_label' => 'id',
            ])
            ->add('id_instructions', EntityType::class, [
                'class' => Instructions::class,
'choice_label' => 'id',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RelRecetteInstructions::class,
        ]);
    }
}
