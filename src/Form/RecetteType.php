<?php

namespace App\Form;

use App\Entity\Recette;
use App\Entity\RecetteCategorie;
use App\Entity\RecetteSousCategorie;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecetteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('temps_preparation')
            ->add('temps_cuisson')
            ->add('id_categorie', EntityType::class, [
                'class' => RecetteCategorie::class,
'choice_label' => 'id',
            ])
            ->add('id_sous_categorie', EntityType::class, [
                'class' => RecetteSousCategorie::class,
'choice_label' => 'id',
'multiple' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Recette::class,
        ]);
    }
}
