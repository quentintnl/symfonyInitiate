<?php

namespace App\Form;

use App\Entity\Recette;
use App\Entity\RelRecetteUstensiles;
use App\Entity\Utensils;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RelRecetteUstensilesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('quantite')
            ->add('id_recette', EntityType::class, [
                'class' => Recette::class,
'choice_label' => 'id',
            ])
            ->add('id_ustensiles', EntityType::class, [
                'class' => Utensils::class,
'choice_label' => 'id',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RelRecetteUstensiles::class,
        ]);
    }
}
