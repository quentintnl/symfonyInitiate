<?php

namespace App\Form;

use App\Entity\Ingredients;
use App\Entity\Recette;
use App\Entity\RelRecetteIngredients;
use App\Entity\Unite;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RelRecetteIngredientsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('quantite')
            ->add('id_recette', EntityType::class, [
                'class' => Recette::class,
'choice_label' => 'id',
            ])
            ->add('id_ingredient', EntityType::class, [
                'class' => Ingredients::class,
'choice_label' => 'id',
            ])
            ->add('id_unite', EntityType::class, [
                'class' => Unite::class,
'choice_label' => 'id',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RelRecetteIngredients::class,
        ]);
    }
}
