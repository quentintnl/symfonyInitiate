<?php

namespace App\Repository;

use App\Entity\RelRecetteIngredients;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RelRecetteIngredients>
 *
 * @method RelRecetteIngredients|null find($id, $lockMode = null, $lockVersion = null)
 * @method RelRecetteIngredients|null findOneBy(array $criteria, array $orderBy = null)
 * @method RelRecetteIngredients[]    findAll()
 * @method RelRecetteIngredients[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelRecetteIngredientsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RelRecetteIngredients::class);
    }

//    /**
//     * @return RelRecetteIngredients[] Returns an array of RelRecetteIngredients objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RelRecetteIngredients
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
