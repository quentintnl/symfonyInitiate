<?php

namespace App\Repository;

use App\Entity\RelRecetteUstensiles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RelRecetteUstensiles>
 *
 * @method RelRecetteUstensiles|null find($id, $lockMode = null, $lockVersion = null)
 * @method RelRecetteUstensiles|null findOneBy(array $criteria, array $orderBy = null)
 * @method RelRecetteUstensiles[]    findAll()
 * @method RelRecetteUstensiles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelRecetteUstensilesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RelRecetteUstensiles::class);
    }

//    /**
//     * @return RelRecetteUstensiles[] Returns an array of RelRecetteUstensiles objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RelRecetteUstensiles
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
