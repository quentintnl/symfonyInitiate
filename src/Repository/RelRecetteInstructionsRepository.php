<?php

namespace App\Repository;

use App\Entity\RelRecetteInstructions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RelRecetteInstructions>
 *
 * @method RelRecetteInstructions|null find($id, $lockMode = null, $lockVersion = null)
 * @method RelRecetteInstructions|null findOneBy(array $criteria, array $orderBy = null)
 * @method RelRecetteInstructions[]    findAll()
 * @method RelRecetteInstructions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RelRecetteInstructionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RelRecetteInstructions::class);
    }

//    /**
//     * @return RelRecetteInstructions[] Returns an array of RelRecetteInstructions objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?RelRecetteInstructions
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
