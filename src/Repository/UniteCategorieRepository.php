<?php

namespace App\Repository;

use App\Entity\UniteCategorie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UniteCategorie>
 *
 * @method UniteCategorie|null find($id, $lockMode = null, $lockVersion = null)
 * @method UniteCategorie|null findOneBy(array $criteria, array $orderBy = null)
 * @method UniteCategorie[]    findAll()
 * @method UniteCategorie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UniteCategorieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UniteCategorie::class);
    }

//    /**
//     * @return UniteCategorie[] Returns an array of UniteCategorie objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?UniteCategorie
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
