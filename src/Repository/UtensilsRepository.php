<?php

namespace App\Repository;

use App\Entity\Utensils;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Utensils>
 *
 * @method Utensils|null find($id, $lockMode = null, $lockVersion = null)
 * @method Utensils|null findOneBy(array $criteria, array $orderBy = null)
 * @method Utensils[]    findAll()
 * @method Utensils[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UtensilsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Utensils::class);
    }

//    /**
//     * @return Utensils[] Returns an array of Utensils objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('u.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Utensils
//    {
//        return $this->createQueryBuilder('u')
//            ->andWhere('u.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
