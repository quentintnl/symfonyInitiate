<?php

namespace App\Entity;

use App\Repository\RecetteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RecetteRepository::class)]
class Recette
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    private ?RecetteCategorie $id_categorie = null;

    #[ORM\ManyToMany(targetEntity: RecetteSousCategorie::class)]
    private Collection $id_sous_categorie;

    #[ORM\Column(length: 255)]
    private ?string $temps_preparation = null;

    #[ORM\Column(length: 255)]
    private ?string $temps_cuisson = null;

    public function __construct()
    {
        $this->id_sous_categorie = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getIdCategorie(): ?RecetteCategorie
    {
        return $this->id_categorie;
    }

    public function setIdCategorie(?RecetteCategorie $id_categorie): static
    {
        $this->id_categorie = $id_categorie;

        return $this;
    }

    /**
     * @return Collection<int, RecetteSousCategorie>
     */
    public function getIdSousCategorie(): Collection
    {
        return $this->id_sous_categorie;
    }

    public function addIdSousCategorie(RecetteSousCategorie $idSousCategorie): static
    {
        if (!$this->id_sous_categorie->contains($idSousCategorie)) {
            $this->id_sous_categorie->add($idSousCategorie);
        }

        return $this;
    }

    public function removeIdSousCategorie(RecetteSousCategorie $idSousCategorie): static
    {
        $this->id_sous_categorie->removeElement($idSousCategorie);

        return $this;
    }

    public function getTempsPreparation(): ?string
    {
        return $this->temps_preparation;
    }

    public function setTempsPreparation(string $temps_preparation): static
    {
        $this->temps_preparation = $temps_preparation;

        return $this;
    }

    public function getTempsCuisson(): ?string
    {
        return $this->temps_cuisson;
    }

    public function setTempsCuisson(string $temps_cuisson): static
    {
        $this->temps_cuisson = $temps_cuisson;

        return $this;
    }
}
