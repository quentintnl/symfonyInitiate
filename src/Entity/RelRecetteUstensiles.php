<?php

namespace App\Entity;

use App\Repository\RelRecetteUstensilesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RelRecetteUstensilesRepository::class)]
class RelRecetteUstensiles
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Recette $id_recette = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Utensils $id_ustensiles = null;

    #[ORM\Column(length: 255)]
    private ?string $quantite = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdRecette(): ?Recette
    {
        return $this->id_recette;
    }

    public function setIdRecette(Recette $id_recette): static
    {
        $this->id_recette = $id_recette;

        return $this;
    }

    public function getIdUstensiles(): ?Utensils
    {
        return $this->id_ustensiles;
    }

    public function setIdUstensiles(Utensils $id_ustensiles): static
    {
        $this->id_ustensiles = $id_ustensiles;

        return $this;
    }

    public function getQuantite(): ?string
    {
        return $this->quantite;
    }

    public function setQuantite(string $quantite): static
    {
        $this->quantite = $quantite;

        return $this;
    }
}
