<?php

namespace App\Entity;

use App\Repository\RecetteSousCategorieRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RecetteSousCategorieRepository::class)]
class RecetteSousCategorie
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?RecetteCategorie $id_categorie = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getIdCategorie(): ?RecetteCategorie
    {
        return $this->id_categorie;
    }

    public function setIdCategorie(RecetteCategorie $id_categorie): static
    {
        $this->id_categorie = $id_categorie;

        return $this;
    }
}
