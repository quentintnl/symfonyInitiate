<?php

namespace App\Entity;

use App\Repository\RelRecetteInstructionsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RelRecetteInstructionsRepository::class)]
class RelRecetteInstructions
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Recette $id_recette = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Instructions $id_instructions = null;

    #[ORM\Column(length: 255)]
    private ?string $ordre = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdRecette(): ?Recette
    {
        return $this->id_recette;
    }

    public function setIdRecette(Recette $id_recette): static
    {
        $this->id_recette = $id_recette;

        return $this;
    }

    public function getIdInstructions(): ?Instructions
    {
        return $this->id_instructions;
    }

    public function setIdInstructions(Instructions $id_instructions): static
    {
        $this->id_instructions = $id_instructions;

        return $this;
    }

    public function getOrdre(): ?string
    {
        return $this->ordre;
    }

    public function setOrdre(string $ordre): static
    {
        $this->ordre = $ordre;

        return $this;
    }
}
