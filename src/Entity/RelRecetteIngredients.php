<?php

namespace App\Entity;

use App\Repository\RelRecetteIngredientsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RelRecetteIngredientsRepository::class)]
class RelRecetteIngredients
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Recette $id_recette = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Ingredients $id_ingredient = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Unite $id_unite = null;

    #[ORM\Column(length: 255)]
    private ?string $quantite = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdRecette(): ?Recette
    {
        return $this->id_recette;
    }

    public function setIdRecette(Recette $id_recette): static
    {
        $this->id_recette = $id_recette;

        return $this;
    }

    public function getIdIngredient(): ?Ingredients
    {
        return $this->id_ingredient;
    }

    public function setIdIngredient(Ingredients $id_ingredient): static
    {
        $this->id_ingredient = $id_ingredient;

        return $this;
    }

    public function getIdUnite(): ?Unite
    {
        return $this->id_unite;
    }

    public function setIdUnite(Unite $id_unite): static
    {
        $this->id_unite = $id_unite;

        return $this;
    }

    public function getQuantite(): ?string
    {
        return $this->quantite;
    }

    public function setQuantite(string $quantite): static
    {
        $this->quantite = $quantite;

        return $this;
    }
}
